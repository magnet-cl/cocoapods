#
# Be sure to run `pod lib lint MNCLLibrary.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "MNCLLibrary"
    s.version          = "0.2.7"
    s.summary          = "MNCLLibrary used as a base for magnet objects."
    s.homepage         = "http://www.magnet.cl"
    # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
    s.license          = 'MIT'
    s.author           = { "benjamín fantini" => "bdfantini@gmail.com" }
    s.source           = { :git => "https://bfantini@bitbucket.org/magnet-cl/magnetlibrary-ios.git", :tag => s.version.to_s }

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.source_files = 'Pod/Classes/**/*'
    s.resource_bundles = {
    'MNCLLibrary' => ['Pod/Assets/*.png']
    }

    s.public_header_files = 'Pod/Classes/**/*.h'
    # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'AFNetworking', '~> 2.5'
    s.dependency 'ObjectiveRecord', '~> 1.5'
    s.dependency 'MBProgressHUD', '~> 0.9'
end
